package com.zarbosoft.automodel.task;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.zarbosoft.automodel.lib.Listener;
import com.zarbosoft.automodel.lib.ModelBase;
import com.zarbosoft.rendaw.common.Assertion;
import org.decimal4j.api.Decimal;

import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static com.zarbosoft.automodel.task.GenerateChange.CHANGE_TOKEN_NAME;
import static com.zarbosoft.automodel.task.Helper.capFirst;
import static com.zarbosoft.automodel.task.Helper.poetJoin;
import static javax.lang.model.element.Modifier.PUBLIC;

public interface AutoType {

  TypeName poet();

  default TypeName poetBoxed() {
    return poet();
  }

  default boolean flattenPoint() {
    return false;
  }

  CodeBlock generateSerializeCode(String expr);

  DeserializeCodeBuilt generateDeserializerCode(String name, String sourceExpr, boolean lazyFinish);

  CodeBlock def();

  void addGettersInto(TypeSpec.Builder clone, String name);

  void extendDecRef(CodeBlock.Builder decRef, String name);

  void generateChangesInto(
      Path path,
      AutoField field,
      String name,
      AutoObject entry,
      List<String> notifySets,
      CodeBlock.Builder versionChangeDeserialize,
      ClassName typeChangeStepBuilderName,
      TypeSpec.Builder typeChangeStepBuilder);

  CodeBlock generateWalk(String name);

  void generateMutatorInto(AutoObject obj, AutoField field, String name, List<String> notifySets);

  void generateCloneInto(CodeBlock.Builder clone, String name);

  CodeBlock generateCloneDeep(String name);

  CodeBlock generateHash(String name);

  static class DeserializeCode {
    public final CodeBlock.Builder value = CodeBlock.builder();
    public final CodeBlock.Builder valueStatements = CodeBlock.builder();
    public final CodeBlock.Builder array = CodeBlock.builder();
    public final CodeBlock.Builder record = CodeBlock.builder();
    public final CodeBlock.Builder finish = CodeBlock.builder();

    public DeserializeCode value(Consumer<CodeBlock.Builder> consumer) {
      consumer.accept(value);
      return this;
    }

    public DeserializeCode valueStatements(Consumer<CodeBlock.Builder> consumer) {
      consumer.accept(valueStatements);
      return this;
    }

    public DeserializeCode array(Consumer<CodeBlock.Builder> consumer) {
      consumer.accept(array);
      return this;
    }

    public DeserializeCode record(Consumer<CodeBlock.Builder> consumer) {
      consumer.accept(record);
      return this;
    }

    public DeserializeCode finish(Consumer<CodeBlock.Builder> consumer) {
      consumer.accept(finish);
      return this;
    }

    public DeserializeCodeBuilt build() {
      return new DeserializeCodeBuilt(
          value.build(), valueStatements.build(), array.build(), record.build(), finish.build());
    }
  }

  static class DeserializeCodeBuilt {
    public final CodeBlock value;
    public final CodeBlock valueStatements;
    public final CodeBlock array;
    public final CodeBlock record;
    public final CodeBlock finish;

    public DeserializeCodeBuilt(
        CodeBlock value,
        CodeBlock valueStatements,
        CodeBlock array,
        CodeBlock record,
        CodeBlock finish) {
      this.value = value;
      this.valueStatements = valueStatements;
      this.array = array;
      this.record = record;
      this.finish = finish;
    }
  }

  static void addScalarGettersInto(TypeSpec.Builder clone, String name, AutoType type) {
    MethodSpec.Builder getter =
        MethodSpec.methodBuilder(name).returns(type.poet()).addModifiers(PUBLIC);
    clone.addMethod(getter.addCode("return $L;\n", name).build());
  }

  static void addScalarMutatorInto(
      AutoObject object, String name, AutoType type, List<String> notifySets) {
    String action = "set";
    GenerateNotifier notifier =
        new GenerateNotifier(
            object,
            "this",
            name + capFirst(action) + "Listeners",
            ParameterizedTypeName.get(
                ClassName.get(Listener.ScalarSet.class), object.genName, type.poetBoxed()));
    notifier.onAddListener("listener.accept(this, $L);\n", new String[] {name});
    notifier.addNotifyArgumentString("this");
    notifier.addNotifyArgumentString("value");
    object.genBuilder.addMethod(
        MethodSpec.methodBuilder(action + capFirst(name))
            .addModifiers(PUBLIC)
            .addParameter(ParameterSpec.builder(type.poet(), "value").build())
            .addCode(CodeBlock.of("this.$L = value;\n", name))
            .addCode(CodeBlock.of("hashUnclean();\n"))
            .addCode(notifier.generateNotify())
            .addCode(
                poetJoin(
                    "",
                    notifySets.stream()
                        .map(s -> GenerateNotifier.generateNotify("this", s + "Listeners"))))
            .build());
    notifier.generate();
  }

  static void generateScalarChangesInto(
      Path path,
      AutoField field,
      String name,
      AutoType type,
      AutoObject entry,
      List<String> notifySets,
      CodeBlock.Builder versionChangeDeserialize,
      ClassName typeChangeStepBuilderName,
      TypeSpec.Builder typeChangeStepBuilder) {
    GenerateChange setBuilder =
        new GenerateChange(
            entry,
            field,
            name,
            "set",
            notifySets,
            ParameterizedTypeName.get(
                ClassName.get(Listener.ScalarSet.class), entry.genName, type.poetBoxed()),
            typeChangeStepBuilderName);
    CodeBlock.Builder changeMerge =
        CodeBlock.builder()
            .add(
                "if (other.getClass() != getClass() || (($T)other).target != target) return false;\n",
                setBuilder.getName());
    if (type.flattenPoint())
      changeMerge.add("value.decRef(context, null); // No deletion should happen here\n");
    changeMerge.add("value = (($T)other).value;\n", setBuilder.getName());
    if (type.flattenPoint()) changeMerge.add("value.incRef(context);\n");
    changeMerge.add("return true;\n");
    setBuilder
        .addParameter(type, "value")
        .addCode("if ($T.equals(value, target.$L)) return;\n", Objects.class, name)
        .addCode(
            "changeStep.add(context, new $T(context, target, target.$L), postChange);\n",
            CHANGE_TOKEN_NAME,
            name);
    if (type.flattenPoint())
      setBuilder.addCode(
          "if (target.$L != null) target.$L.decRef(context, postChange);\n", name, name);
    setBuilder.addCode("target.$L = value;\n", name);
    if (type.flattenPoint()) setBuilder.addCode("if (value != null) value.incRef(context);\n");
    setBuilder
        .onAddListener("listener.accept(this, $L);\n", name)
        .mergeAdd(changeMerge.build())
        .finish(path, typeChangeStepBuilder, versionChangeDeserialize);

    CodeBlock.Builder initialSetCode =
        CodeBlock.builder()
            .add("if (refCount > 0) throw new $T();\n", Assertion.class)
            .add("this.$L = value;\n", name);
    if (type.flattenPoint()) initialSetCode.add("value.incRef(context);\n");
    entry.genBuilder.addMethod(
        MethodSpec.methodBuilder(String.format("initial%sSet", capFirst(name)))
            .addModifiers(PUBLIC)
            .addParameter(ModelBase.class, "context")
            .addParameter(type.poet(), "value")
            .addCode(initialSetCode.build())
            .build());
    if (!type.flattenPoint()) {
      entry.genBuilder.addMethod(
          MethodSpec.methodBuilder(String.format("forceInitial%sSet", capFirst(name)))
              .addModifiers(PUBLIC)
              .addParameter(type.poet(), "value")
              .addCode(CodeBlock.builder().add("this.$L = value;\n", name).build())
              .build());
    }
  }

  public abstract static class PrimitiveAutoType implements AutoType {
    @Override
    public final void addGettersInto(TypeSpec.Builder clone, String name) {
      addScalarGettersInto(clone, name, this);
    }

    @Override
    public final void extendDecRef(CodeBlock.Builder decRef, String name) {}

    @Override
    public final void generateChangesInto(
        Path path,
        AutoField field,
        String name,
        AutoObject entry,
        List<String> notifySets,
        CodeBlock.Builder versionChangeDeserialize,
        ClassName typeChangeStepBuilderName,
        TypeSpec.Builder typeChangeStepBuilder) {
      generateScalarChangesInto(
          path,
          field,
          name,
          this,
          entry,
          notifySets,
          versionChangeDeserialize,
          typeChangeStepBuilderName,
          typeChangeStepBuilder);
    }

    @Override
    public final CodeBlock generateWalk(String name) {
      return null;
    }

    @Override
    public final void generateMutatorInto(
        AutoObject obj, AutoField field, String name, List<String> notifySets) {
      addScalarMutatorInto(obj, name, this, notifySets);
    }

    @Override
    public final void generateCloneInto(CodeBlock.Builder clone, String name) {
      clone.add("out.$L = $L;\n", name, name);
    }

    @Override
    public final CodeBlock generateCloneDeep(String name) {
      return CodeBlock.of("$L", name);
    }

    @Override
    public CodeBlock generateHash(String name) {
      return CodeBlock.of("$L", name);
    }
  }

  public static AutoType string =
      new PrimitiveAutoType() {
        @Override
        public TypeName poet() {
          return TypeName.get(String.class);
        }

        @Override
        public CodeBlock generateSerializeCode(String expr) {
          return CodeBlock.of("writer.primitive($L);\n", expr);
        }

        @Override
        public DeserializeCodeBuilt generateDeserializerCode(
            String name, String sourceExpr, boolean lazyFinish) {
          return new DeserializeCode()
              .value(b -> b.add("($T) $L", String.class, sourceExpr))
              .build();
        }

        @Override
        public CodeBlock def() {
          return CodeBlock.of("null");
        }
      };
  public static AutoType integer =
      new PrimitiveAutoType() {

        @Override
        public TypeName poet() {
          return TypeName.INT;
        }

        @Override
        public TypeName poetBoxed() {
          return TypeName.get(Integer.class);
        }

        @Override
        public CodeBlock generateSerializeCode(String expr) {
          return CodeBlock.of("writer.primitive($T.toString($L));\n", Integer.class, expr);
        }

        @Override
        public DeserializeCodeBuilt generateDeserializerCode(
            String name, String sourceExpr, boolean lazyFinish) {
          return new DeserializeCode()
              .value(b -> b.add("$T.valueOf(($T) $L)", Integer.class, String.class, sourceExpr))
              .build();
        }

        @Override
        public CodeBlock def() {
          return CodeBlock.of("$L", 0);
        }
      };
  public static AutoType lon =
      new PrimitiveAutoType() {

        @Override
        public TypeName poet() {
          return TypeName.LONG;
        }

        @Override
        public TypeName poetBoxed() {
          return TypeName.get(Long.class);
        }

        @Override
        public CodeBlock generateSerializeCode(String expr) {
          return CodeBlock.of("writer.primitive($T.toString($L));\n", Long.class, expr);
        }

        @Override
        public DeserializeCodeBuilt generateDeserializerCode(
            String name, String sourceExpr, boolean lazyFinish) {
          return new DeserializeCode()
              .value(b -> b.add("$T.valueOf(($T) $L)", Long.class, String.class, sourceExpr))
              .build();
        }

        @Override
        public CodeBlock def() {
          return CodeBlock.of("$L", 0l);
        }
      };
  public static AutoType bool =
      new PrimitiveAutoType() {

        @Override
        public TypeName poet() {
          return TypeName.BOOLEAN;
        }

        @Override
        public TypeName poetBoxed() {
          return TypeName.get(Boolean.class);
        }

        @Override
        public CodeBlock generateSerializeCode(String expr) {
          return CodeBlock.of("writer.primitive($L ? \"true\" : \"false\");\n", expr);
        }

        @Override
        public DeserializeCodeBuilt generateDeserializerCode(
            String name, String sourceExpr, boolean lazyFinish) {
          return new DeserializeCode().value(b -> b.add("\"true\".equals($L)", sourceExpr)).build();
        }

        @Override
        public CodeBlock def() {
          return CodeBlock.of("$L", false);
        }
      };

  public static AutoType of(Class klass) {
    return new PrimitiveAutoType() {
      @Override
      public TypeName poet() {
        return TypeName.get(klass);
      }

      @Override
      public CodeBlock generateSerializeCode(String expr) {
        return CodeBlock.of("$L.serialize(writer);\n", expr);
      }

      @Override
      public DeserializeCodeBuilt generateDeserializerCode(
          String name, String sourceExpr, boolean lazyFinish) {
        return new DeserializeCode()
            .record(b -> b.add("new $T.Deserializer()", TypeName.get(klass)))
            .value(b -> b.add("($T) $L", TypeName.get(klass), sourceExpr))
            .build();
      }

      @Override
      public CodeBlock def() {
        return null;
      }
    };
  }

  public static AutoType ofDec(Class<? extends Decimal> klass) {
    return new PrimitiveAutoType() {
      @Override
      public TypeName poet() {
        return TypeName.get(klass);
      }

      @Override
      public CodeBlock generateSerializeCode(String expr) {
        return CodeBlock.of("writer.primitive($L.toString());\n", expr);
      }

      @Override
      public DeserializeCodeBuilt generateDeserializerCode(
          String name, String sourceExpr, boolean lazyFinish) {
        return new DeserializeCode()
            .value(b -> b.add("$T.valueOf(($T) $L)", klass, String.class, sourceExpr))
            .build();
      }

      @Override
      public CodeBlock def() {
        return CodeBlock.of("$T.ZERO", klass);
      }
    };
  }

  public static AutoType ofEnum(Class<? extends Enum> klass) {
    return new PrimitiveAutoType() {
      @Override
      public TypeName poet() {
        return TypeName.get(klass);
      }

      @Override
      public CodeBlock generateSerializeCode(String expr) {
        return CodeBlock.of("writer.primitive($L.toString());\n", expr);
      }

      @Override
      public DeserializeCodeBuilt generateDeserializerCode(
          String name, String sourceExpr, boolean lazyFinish) {
        DeserializeCode out = new DeserializeCode();
        out.value.add("(\n").indent();
        for (Object value : klass.getEnumConstants()) {
          out.value.add("$L.equals($S) ? $T.$L :\n", sourceExpr, value, klass, value.toString());
        }
        out.value.add(
            "(new $T<$T>() { public $T get() { throw new $T(); } }).get()\n",
            Supplier.class,
            klass,
            klass,
            Assertion.class);
        out.value.unindent().add(")");
        return out.build();
      }

      @Override
      public CodeBlock def() {
        return null;
      }
    };
  }
}
