package com.zarbosoft.automodel.task;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.function.Consumer;

public class Poetry {
  public static class PoetryPair {
    public final ClassName name;
    public final TypeSpec.Builder builder;

    public PoetryPair(ClassName name, TypeSpec.Builder builder) {
      this.name = name;
      this.builder = builder;
    }

    public PoetryPair(ClassName name) {
      this.name = name;
      this.builder = TypeSpec.classBuilder(name);
    }
  }

}
