package com.zarbosoft.automodel.task;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.zarbosoft.rendaw.common.Assertion;

import javax.lang.model.element.Modifier;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;

public class AutoEnum extends AutoType.PrimitiveAutoType {
  final ClassName genName;
  final ArrayList<String> values;

  public AutoEnum(AutoModelVersion version, String name, String[] values) {
    genName = version.name(name);
    this.values = new ArrayList<>(Arrays.asList(values));
  }

  public AutoEnum(AutoModelVersion version, String name) {
    genName = version.name(name);
    this.values = new ArrayList<>();
  }

  public AutoEnum value(String value) {
    this.values.add(value);
    return this;
  }

  @Override
  public TypeName poet() {
    return genName;
  }

  @Override
  public CodeBlock generateSerializeCode(String expr) {
    CodeBlock.Builder builder = CodeBlock.builder();
    builder.add("switch ($L) {\n", expr).indent();
    for (String value : values) {
      builder.add("case $L: writer.primitive($S); break;\n", value.toUpperCase(), value);
    }
    builder.add("default: throw new $T();\n", Assertion.class);
    builder.unindent().add("}\n");
    return builder.build();
  }

  @Override
  public DeserializeCodeBuilt generateDeserializerCode(
      String name, String sourceExpr, boolean lazyFinish) {
    DeserializeCode out = new DeserializeCode();
    out.value.add("(\n").indent();
    for (String value : values) {
      out.value.add("$L.equals($S) ? $T.$L :\n", sourceExpr, value, genName, value.toUpperCase());
    }
    out.value.add(
        "(new $T<$T>() { public $T get() { throw new $T(); } }).get()\n",
        Supplier.class,
        genName,
        genName,
        Assertion.class);
    out.value.unindent().add(")");
    return out.build();
  }

  @Override
  public CodeBlock def() {
    return CodeBlock.of("$T.$L", genName, values.get(0).toUpperCase());
  }

  void generate(Path path) {
    TypeSpec.Builder out = TypeSpec.enumBuilder(genName).addModifiers(Modifier.PUBLIC);
    for (String v : values) {
      out.addEnumConstant(v.toUpperCase());
    }
    Helper.write(path, genName, out.build());
  }
}
