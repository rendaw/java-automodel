package com.zarbosoft.automodel.task;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.zarbosoft.rendaw.common.Assertion;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class AutoField {
  final AutoObject parent;
  CodeBlock def;
  boolean persist;
  Mutability mutability;
  final List<String> notifySets = new ArrayList<>();

  void generateMutatorInto(AutoField sourceField, AutoObject obj) {
    type.generateMutatorInto(obj, sourceField, name, notifySets);
  }

  public void generateClone(CodeBlock.Builder clone) {
    type.generateCloneInto(clone, name);
  }

  public CodeBlock generateHash() {
    return type.generateHash(name);
  }

  public void generateCloneDeep(CodeBlock.Builder clone) {
    clone.add(
      "out.$L = $L;\n", name, type.generateCloneDeep(name));
    if (type.flattenPoint()) {
      clone.add("out.$L.incRef(model);\n", name);
    }
  }

  public boolean influencesHash() {
    return persist && (mutability == Mutability.MUTABLE || mutability == Mutability.VERSIONED);
  }

  public static enum Mutability {
    READONLY,
    MUTABLE,
    VERSIONED
  }

  public AutoField def(CodeBlock block) {
    def = block;
    return this;
  }

  public AutoField def(String format, Object... args) {
    def = CodeBlock.of(format, args);
    return this;
  }

  public AutoField notify(String set) {
    notifySets.add(set);
    return this;
  }

  CodeBlock generateSerialize() {
    return CodeBlock.of("writer.key($S);\n$L", name, type.generateSerializeCode(name));
  }

  AutoType.DeserializeCodeBuilt generateDeserializerCode(boolean lazyFinish) {
    AutoType.DeserializeCodeBuilt pre = type.generateDeserializerCode(name, "value", lazyFinish);
    AutoType.DeserializeCode out = new AutoType.DeserializeCode();
    if (!pre.record.isEmpty()) {
      out.record.add("case \"$L\": return $L;\n", name, pre.record);
    }
    if (!pre.array.isEmpty()) {
      out.array.add("case \"$L\": return $L;\n", name, pre.array);
    }
    if (!pre.value.isEmpty() || !pre.valueStatements.isEmpty()) {
      out.value.add("case \"$L\":\n", name).indent();
      if (!pre.valueStatements.isEmpty()) out.value.add(pre.valueStatements);
      if (!pre.value.isEmpty()) out.value.add("out.$L = $L;\n", name, pre.value);
      out.value.add("break;\n").unindent();
    }
    out.finish.add(pre.finish);
    return out.build();
  }

  public String name;
  public AutoType type;
  final List<String> comments;
  public AutoType key;

  public AutoField comment(String comment) {
    this.comments.add(comment);
    return this;
  }

  public AutoField(AutoObject parent, String name, AutoType type) {
    this.parent = parent;
    this.name = name;
    this.type = type;
    this.persist = false;
    this.mutability = Mutability.READONLY;
    this.comments = new ArrayList<>();
    this.notify("any");
  }

  public AutoField versioned() {
    if (mutability != Mutability.READONLY) throw new Assertion();
    persist = true;
    mutability = Mutability.VERSIONED;
    return this;
  }

  public AutoField persist() {
    persist = true;
    return this;
  }

  void addGettersInto(TypeSpec.Builder clone) {
    type.addGettersInto(clone, name);
  }

  void extendDecRef(CodeBlock.Builder decRef) {
    type.extendDecRef(decRef, name);
  }

  void generateChangesInto(
      Path path,
      AutoObject entry,
      CodeBlock.Builder globalChangeDeserialize,
      ClassName typeChangeStepBuilderName,
      TypeSpec.Builder typeChangeStepBuilder) {
    type.generateChangesInto(
        path,
        this,
        name,
        entry,
        notifySets,
        globalChangeDeserialize,
        typeChangeStepBuilderName,
        typeChangeStepBuilder);
  }

  CodeBlock generateWalk() {
    return type.generateWalk(name);
  }

  public AutoField mutable() {
    if (mutability != Mutability.READONLY) throw new Assertion();
    this.mutability = Mutability.MUTABLE;
    return this;
  }
}
