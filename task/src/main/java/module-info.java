open module com.zarbosoft.automodel.task {
  requires javapoet;
  requires com.zarbosoft.rendaw.common;
  requires java.compiler;
  requires com.zarbosoft.automodel.lib;
  requires com.zarbosoft.interface1;
  requires com.zarbosoft.luxem;
  requires decimal4j;
  exports com.zarbosoft.automodel.task;
}