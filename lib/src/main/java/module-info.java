open module com.zarbosoft.automodel.lib {
  requires com.zarbosoft.luxem;
  requires com.zarbosoft.appdirsj;
  requires com.google.common;
  requires com.zarbosoft.rendaw.common;
  requires com.zarbosoft.interface1;
  exports com.zarbosoft.automodel.lib;
}