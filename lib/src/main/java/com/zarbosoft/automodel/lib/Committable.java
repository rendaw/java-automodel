package com.zarbosoft.automodel.lib;

public interface Committable {
  void commit(ModelBase context);
}
