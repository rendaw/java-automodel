package com.zarbosoft.automodel.lib;

import com.zarbosoft.luxem.write.RawWriter;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

public abstract class ProjectObject {
  protected long id;
  protected int refCount;
  public List<ProjectObject> parents = new ArrayList<>();
  protected int hash;
  public boolean hashClean = false;

  public long id() {
    return id;
  }

  public int refCount() {
    return refCount;
  }

  public abstract void incRef(ModelBase context);

  public abstract void decRef(ModelBase context, List<Runnable> postChange);

  public abstract void serialize(RawWriter writer);

  public abstract void walk(Deque<Iterator<? extends ProjectObject>> queue);

  public abstract ProjectObject clone_(ModelBase model);

  public abstract ProjectObject cloneDeep(ModelBase model);

  public abstract int myHash();

  public static class FlatDeque<T> {
    Deque<Iterator<T>> inner = new ArrayDeque<>();

    public void push(Iterator<T> next) {
      if (!next.hasNext()) return;
      inner.addLast(next);
    }

    public boolean isEmpty() {
      return inner.isEmpty();
    }

    public T next() {
      Iterator<T> nextIter = inner.peekLast();
      T out = nextIter.next();
      if (!nextIter.hasNext()) inner.removeLast();
      return out;
    }
  }

  public void hashUnclean() {
    hashClean = false;
    FlatDeque<ProjectObject> queue = new FlatDeque<>();
    queue.push(parents.iterator());
    while (!queue.isEmpty()) {
      ProjectObject ancestor = queue.next();
      if (!ancestor.hashClean) continue;
      ancestor.hashClean = false;
      queue.push(ancestor.parents.iterator());
    }
  }

  protected static long takeId(ModelBase context) {
    return context.nextId++;
  }
}
