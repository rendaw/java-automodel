package com.zarbosoft.automodel.lib;

import com.zarbosoft.luxem.read.StackReader;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.DeadCode;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ModelVersionDeserializer extends StackReader.RecordState
    implements DeserializeContext {
  // Persistent
  public Map<Long, ProjectObject> objectMap = new HashMap<>();
  protected long nextId = 0;
  protected ModelSnapshot current = null;
  protected List<ModelSnapshot> snapshots = new ArrayList<>();

  // State
  protected final int maxUndo;
  protected final Path path;

  public final List<Finisher> finishers = new ArrayList<>();

  @Override
  public <T> T getObject(Long key) {
    if (key == null) return null;
    return (T)
        objectMap.computeIfAbsent(
            key,
            k -> {
              throw new IllegalStateException(
                  String.format("Can't find object %s in saved data.", key));
            });
  }

  public abstract StackReader.State deserializeObject(
      ModelVersionDeserializer context, String type);

  public ModelVersionDeserializer(Path path, int maxUndo) {
    this.maxUndo = maxUndo;
    this.path = path;
  }

  @Override
  public void value(Object value) {
    if ("nextId".equals(key)) {
      nextId = Long.parseLong((String) value);
    } else if ("current".equals(key)) {
      current = (ModelSnapshot) value;
    } else if ("snapshots".equals(key)) {
      snapshots = (List<ModelSnapshot>) value;
    } else if ("objects".equals(key)) {
    } else throw new RuntimeException(String.format("Unknown model key %s", key));
  }

  @Override
  public StackReader.State array() {
    if (false) {
      throw new DeadCode();
    } else if ("objects".equals(key)) {
      return new StackReader.ArrayState() {
        @Override
        public StackReader.State record() {
          if (type == null) throw new IllegalStateException("Object has no type!");
          return deserializeObject(ModelVersionDeserializer.this, type);
        }
      };
    } else if ("snapshots".equals(key)) {
      return new StackReader.ArrayState() {
        @Override
        public StackReader.State record() {
          return new ModelSnapshot.Deserializer(ModelVersionDeserializer.this);
        }
      };
    } else if ("undo".equals(key)) {
      return new StackReader.ArrayState() {
        @Override
        public void value(Object value) {
          super.value(Long.parseLong((String) value));
        }
      };
    } else if ("redo".equals(key)) {
      return new StackReader.ArrayState() {
        @Override
        public void value(Object value) {
          super.value(Long.parseLong((String) value));
        }
      };
    } else throw new Assertion();
  }

  @Override
  public StackReader.State record() {
    if ("current".equals(key)) {
      return new ModelSnapshot.Deserializer(this);
    } else throw new Assertion();
  }

  public abstract ModelBase generate();

  @Override
  public Object get() {
    final ModelBase out = generate();
    finishers.forEach(finisher -> finisher.finish(this, out));
    boolean fixed = out.debugCheckRefsFix();
    out.debugCheckRefs();
    return new ModelBase.DeserializeResult(fixed, out);
  }

  public abstract Class rootType();

  public abstract static class Finisher {
    public abstract void finish(ModelVersionDeserializer context, ModelBase model);
  }
}
