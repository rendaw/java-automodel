package com.zarbosoft.automodel.lib;

import com.zarbosoft.interface1.Configuration;
import com.zarbosoft.luxem.write.RawWriter;

import java.util.List;
import java.util.function.Consumer;

@Configuration
public abstract class Change {
  public abstract void delete(ModelBase context, List<Runnable> postChange);

  public abstract void apply(ModelBase context, ChangeStep changeStep, List<Runnable> postChange);

  public abstract void serialize(RawWriter writer);

  public abstract void debugRefCounts(Consumer<ProjectObject> increment);

  public abstract boolean merge(ModelBase context, Change other);

  public abstract Change clone(ModelBase context);
}
