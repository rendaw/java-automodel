package com.zarbosoft.automodel.lib;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Streams;
import com.zarbosoft.luxem.read.StackReader;
import com.zarbosoft.luxem.write.RawWriter;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Common;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.zarbosoft.automodel.lib.Logger.logger;
import static com.zarbosoft.rendaw.common.Common.atomicWrite;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class ModelBase implements Committable, DeserializeContext {
  // Persistent
  long nextId;

  // Session
  public final Map<Long, ProjectObject> objectMap;
  public ModelSnapshot current;
  public List<ModelSnapshot> snapshots;
  public final ReadWriteLock lock = new ReentrantReadWriteLock();
  Committer committer = new Committer(lock, this);
  public final Path path;
  final Path changesDir;
  final String vid;

  public static Path projectPath(Path base) {
    return base.resolve("project.luxem");
  }

  public void addUndoSizeListener(Consumer<Integer> listener) {
    current.history.addUndoSizeListener(listener);
  }

  public void addRedoSizeListener(Consumer<Integer> listener) {
    current.history.addRedoSizeListener(listener);
  }

  public void removeUndoSizeListener(Consumer<Integer> listener) {
    current.history.removeUndoSizeListener(listener);
  }

  public void removeRedoSizeListener(Consumer<Integer> listener) {
    current.history.removeRedoSizeListener(listener);
  }

  public void setDirty(Committable object) {
    committer.setDirty(object);
  }

  public abstract StackReader.State deserializeChange(String type);

  @Override
  public <T> T getObject(Long key) {
    if (key == null) return null;
    return (T)
        objectMap.computeIfAbsent(
            key,
            k -> {
              throw new IllegalStateException(
                  String.format("Can't find object %s in saved data.", key));
            });
  }

  public ModelBase(
      Path path,
      long nextId,
      Map<Long, ProjectObject> objectMap,
      ModelSnapshot current,
      List<ModelSnapshot> snapshots,
      String vid) {
    this.path = path;
    this.nextId = nextId;
    changesDir = path.resolve("changes");
    this.objectMap = objectMap;
    this.vid = vid;
    this.current = current;
    this.snapshots = snapshots;
    uncheck(
        () -> {
          Files.createDirectories(path);
          Files.createDirectories(changesDir);
        });
  }

  public static class TestMarkerArg {}

  /**
   * Constructor for running tests
   *
   * @param a
   */
  public ModelBase(TestMarkerArg a) {
    objectMap = new HashMap<>();
    path = null;
    changesDir = null;
    vid = "test";
    current = new ModelSnapshot();
    current.history =
        History.createFromDeserialize(this, new ArrayList<>(), new ArrayList<>(), null, 0);
  }

  public void close() {
    committer.commitAll();
  }

  public void undo() {
    current.history.undo();
  }

  public void redo() {
    current.history.redo();
  }

  public void finishChange() {
    current.history.finishChange();
  }

  public void clearHistory() {
    current.history.clearHistory();
  }

  public void change(
      History.Tuple unique, List<Runnable> postChange, Consumer<ChangeStep> consumer) {
    current.history.change(unique, postChange, consumer);
  }

  public boolean debugCheckRefsFix() {
    return debugCheckRefs(true);
  }

  public void debugCheckRefs() {
    debugCheckRefs(false);
  }

  protected void walkTree(ProjectObject root, Function<ProjectObject, Boolean> handler) {
    Deque<Iterator<? extends ProjectObject>> d = new ArrayDeque<>();
    d.addLast(ImmutableList.of(root).iterator());
    while (!d.isEmpty()) {
      Iterator<? extends ProjectObject> iter = d.getFirst();
      if (!iter.hasNext()) {
        d.removeFirst();
        continue;
      }
      ProjectObject o = iter.next();
      boolean recurse = handler.apply(o);
      if (!recurse) continue;
      o.walk(d);
    }
  }

  @Override
  public void commit(ModelBase context) {
    debugCheckRefs();
    atomicWrite(
        projectPath(path),
        dest -> {
          RawWriter writer = new RawWriter(dest, (byte) ' ', 4);
          writer.type(vid);
          writer.recordBegin();
          writer.key("nextId").primitive(Long.toString(nextId));
          writer.key("current");
          current.serialize(writer);
          writer.key("snapshots").arrayBegin();
          snapshots.forEach(s -> s.serialize(writer));
          writer.arrayEnd();
          writer.key("objects").arrayBegin();
          for (ProjectObject object : objectMap.values()) object.serialize(writer);
          writer.arrayEnd();
          writer.recordEnd();
        });
  }

  public boolean debugCheckRefs(boolean fix) {
    Common.Mutable<Boolean> hadErrors = new Common.Mutable<>(false);
    Consumer<Function<ProjectObject, Boolean>> walkAll =
        consumer -> {
          Streams.concat(Stream.of(current), snapshots.stream())
              .forEach(
                  s -> {
                    walkTree(s.root, consumer);
                    s.history.changeStep.changes.forEach(
                        change -> {
                          change.debugRefCounts(c -> walkTree(c, consumer));
                        });
                    for (Iterator<ChangeStep.CacheId> i = s.history.undoHistory.iterator();
                        i.hasNext(); ) {
                      ChangeStep.CacheId id = i.next();
                      ChangeStep step = s.history.get(id, fix);
                      if (step == null) {
                        i.remove();
                        continue;
                      }
                      step.changes.forEach(
                          change -> {
                            change.debugRefCounts(c -> walkTree(c, consumer));
                          });
                    }
                    for (Iterator<ChangeStep.CacheId> i = s.history.redoHistory.iterator();
                        i.hasNext(); ) {
                      ChangeStep.CacheId id = i.next();
                      ChangeStep step = s.history.get(id, fix);
                      if (step == null) {
                        i.remove();
                        continue;
                      }
                      step.changes.forEach(
                          change -> change.debugRefCounts(c -> walkTree(c, consumer)));
                    }
                  });
        };

    // Sum expected reference counts
    Map<Long, Long> counts = new HashMap<>();
    Function<ProjectObject, Boolean> incCount =
        o -> {
          Long count = counts.get(o.id());
          if (count != null) {
            counts.put(o.id(), count + 1);
            return false;
          }
          counts.put(o.id(), 1L);
          return true;
        };
    walkAll.accept(incCount);

    // Compare (and fix) actual reference counts
    List<Runnable> postChange = new ArrayList<>();
    {
      // If fixing ref counts, start at root and move to leaves - leaves never modify other counts
      // but if a leaf is decremented first and reaches 0, it will go negative if/when parents dec
      // and also reach 0
      LinkedHashMap<ProjectObject, Void> treeSorted0 = new LinkedHashMap<>(objectMap.size());
      Streams.concat(Stream.of(current), snapshots.stream())
          .forEach(
              s -> {
                walkTree(
                    s.root,
                    o -> {
                      treeSorted0.remove(o);
                      treeSorted0.put(o, null);
                      return true;
                    });
              });
      List<ProjectObject> treeSorted = new ArrayList<>(treeSorted0.keySet());
      {
        Collections.reverse(treeSorted);
        Set<Long> seen = new HashSet<>();
        for (Iterator<ProjectObject> iter = treeSorted.iterator(); iter.hasNext(); ) {
          ProjectObject o = iter.next();
          if (seen.contains(o.id())) iter.remove();
          seen.add(o.id());
        }
        Collections.reverse(treeSorted);
      }
      for (ProjectObject o : treeSorted) {
        long got = o.refCount();
        long expected = counts.getOrDefault(o.id(), 0L);
        if (got != expected) {
          hadErrors.value = true;
          String error =
              String.format("Ref count for %s id %s : %s should be %s", o, o.id(), got, expected);
          if (fix) {
            logger.write(error);
            long diff = expected - got;
            if (diff >= 0)
              for (long i = 0; i < diff; ++i) {
                o.incRef(this);
              }
            else
              for (long i = 0; i < -diff; ++i) {
                o.decRef(this, postChange);
              }
          } else {
            throw new Assertion(error);
          }
        }
      }
    }

    // Check rootedness
    {
      Set<Long> seen = new HashSet<>();
      walkAll.accept(
          (ProjectObject o) -> {
            if (!objectMap.containsKey(o.id())) {
              hadErrors.value = true;
              String error = String.format("%s (id %s) missing from object map", o, o.id());
              if (fix) {
                objectMap.put(o.id(), o);
                logger.write(error);
              } else {
                throw new Assertion(error);
              }
            }
            if (seen.contains(o.id())) return false;
            seen.add(o.id());
            return true;
          });
    }

    for (Runnable runnable : postChange) {
      runnable.run();
    }

    return hadErrors.value;
  }

  public static DateTimeFormatter niceStamp = DateTimeFormatter.ofPattern("uuuu MMM d hh:mm a");

  public void snapshot() {
    ModelSnapshot snapshot = current.clone(this);
    snapshots.add(snapshot);
    setDirty(this);
  }

  public void restore(ModelSnapshot snapshot) {
    committer.commitAll();
    lock.writeLock().lock();
    try {
      current.name = LocalDateTime.now().format(niceStamp);
      snapshots.add(current);
      current = snapshot.clone(this);
      current.name = null;
      setDirty(this);
    } finally {
      lock.writeLock().unlock();
    }
  }

  public void deleteSnapshot(ModelSnapshot snapshot) {
    snapshots.remove(snapshot);
    snapshot.history.clearHistory();
    List<Runnable> postChange = new ArrayList<>();
    snapshot.root.decRef(this, postChange);
    for (Runnable runnable : postChange) {
      runnable.run();
    }
    setDirty(this);
  }

  public int undoSize() {
    return current.history.undoHistory.size();
  }

  public int redoSize() {
    return current.history.redoHistory.size();
  }

  public abstract boolean needsMigrate();

  public abstract ModelBase migrate();

  public void commitCurrentChange() {
    setDirty(current.history.changeStep);
  }

  public static class DeserializeResult {
    public final boolean fixed;
    public final ModelBase model;

    public DeserializeResult(boolean fixed, ModelBase model) {
      this.fixed = fixed;
      this.model = model;
    }
  }

  public static DeserializeResult deserialize(
      Path path, Function<String, StackReader.State> handleVersion) {
    return uncheck(
        () -> {
          try (InputStream source = Files.newInputStream(projectPath(path))) {
            return (DeserializeResult)
                new StackReader()
                    .read(
                        source,
                        new StackReader.ArrayState() {
                          @Override
                          public StackReader.State array() {
                            throw new IllegalStateException("Project data should be a record.");
                          }

                          @Override
                          public void value(Object value) {
                            data.add(value);
                          }

                          @Override
                          public StackReader.State record() {
                            if (type == null)
                              throw new IllegalStateException("Project has no version");
                            return handleVersion.apply(type);
                          }
                        })
                    .get(0);
          }
        });
  }
}
