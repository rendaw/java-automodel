package com.zarbosoft.automodel.lib;

import com.zarbosoft.luxem.read.StackReader;
import com.zarbosoft.luxem.write.RawWriter;

import java.time.LocalDateTime;
import java.util.List;

import static com.zarbosoft.automodel.lib.ModelBase.niceStamp;

public class ModelSnapshot {
  public String name;
  public ProjectObject root;
  public History history;

  public ModelSnapshot() {

  }

  public ModelSnapshot clone(ModelBase model) {
    ModelSnapshot snapshot = new ModelSnapshot();
    snapshot.root = root.cloneDeep(model);
    snapshot.root.incRef(model);
    snapshot.name = LocalDateTime.now().format(niceStamp);
    snapshot.history = history.clone_();
    return snapshot;
  }

  public void serialize(RawWriter writer) {
    writer.recordBegin();
    if (name != null) writer.key("name").primitive(name);
    writer.key("id").primitive(Long.toString(root.id()));
    writer.key("activeChange").primitive(Long.toString(history.changeStep.cacheId.id));
    history.serialize(writer);
    writer.recordEnd();
  }

  public static class Deserializer extends StackReader.RecordState {
    Long activeChange = null;
    long id;
    public List<Long> undo;
    public List<Long> redo;
    ModelSnapshot out = new ModelSnapshot();

    public Deserializer(ModelVersionDeserializer context) {
      context.finishers.add(
          new ModelVersionDeserializer.Finisher() {
            @Override
            public void finish(ModelVersionDeserializer context, ModelBase model) {
              out.root = context.getObject(id);
              out.history = History.createFromDeserialize(model, undo, redo, activeChange, context.maxUndo);
            }
          });
    }

    @Override
    public Object get() {
      return out;
    }

    @Override
    public void value(Object value) {
      if ("name".equals(key)) {
        out.name = (String) value;
      } else if ("id".equals(key)) {
        id = Long.parseLong((String) value);
      } else if ("undo".equals(key)) {
        undo = (List<Long>) value;
      } else if ("redo".equals(key)) {
        redo = (List<Long>) value;
      } else if ("activeChange".equals(key)) {
        activeChange = Long.parseLong((String) value);
      } else throw new RuntimeException(String.format("No known field named %s", key));
    }

    @Override
    public StackReader.State array() {
      if ("undo".equals(key)) {
        return new StackReader.ArrayState() {
          @Override
          public void value(Object value) {
            super.value(Long.parseLong((String) value));
          }
        };
      } else if ("redo".equals(key)) {
        return new StackReader.ArrayState() {
          @Override
          public void value(Object value) {
            super.value(Long.parseLong((String) value));
          }
        };
      } else throw new RuntimeException(String.format("No known array field named %s", key));
    }
  }
}
