package com.zarbosoft.automodel.lib;

import com.zarbosoft.luxem.read.StackReader;
import com.zarbosoft.luxem.write.RawWriter;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Common;
import com.zarbosoft.rendaw.common.WeakCache;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.zarbosoft.automodel.lib.Logger.logger;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class History {
  static final WeakCache<ChangeStep.CacheId, ChangeStep> stepLookup =
      new WeakCache<>(c -> c.cacheId);
  public final List<ChangeStep.CacheId> undoHistory;
  public final List<ChangeStep.CacheId> redoHistory;
  private final ModelBase context;
  public ChangeStep changeStep;
  private boolean inChange = false;
  private int maxUndo;
  private WeakList<Consumer<Integer>> undoSizeListeners = new WeakList<>();
  private WeakList<Consumer<Integer>> redoSizeListeners = new WeakList<>();

  public void setMaxUndo(int value) {
    this.maxUndo = value;
  }

  public History clone_() {
    List<ChangeStep.CacheId> undo = new ArrayList<>();
    List<ChangeStep.CacheId> redo = new ArrayList<>();
    undoHistory.stream()
        .forEach(
            id -> {
              ChangeStep change = get(id).clone_(context);
              context.setDirty(change);
              undo.add(change.cacheId);
            });
    redoHistory.stream()
        .forEach(
            id -> {
              ChangeStep change = get(id).clone_(context);
              context.setDirty(change);
              redo.add(change.cacheId);
            });
    ChangeStep clone = changeStep.clone_(context);
    context.setDirty(clone);
    return new History(context, undo, redo, clone, maxUndo);
  }

  public static History createNew(ModelBase context, int maxUndo) {
    return new History(
        context,
        new ArrayList(),
        new ArrayList(),
        new ChangeStep(new ChangeStep.CacheId(context.nextId++)),
        maxUndo);
  }

  public static History createFromDeserialize(
      ModelBase context, List<Long> undo, List<Long> redo, Long activeChange, int maxUndo) {
    ChangeStep changeStep;
    if (activeChange == null) {
      changeStep = new ChangeStep(new ChangeStep.CacheId(context.nextId++));
    } else {
      changeStep = getNullable(context, new ChangeStep.CacheId(activeChange));
      if (changeStep == null) {
        changeStep = new ChangeStep(new ChangeStep.CacheId(context.nextId++));
      }
    }
    List<ChangeStep.CacheId> undoHistory =
        undo.stream()
            .map(i -> new ChangeStep.CacheId(i))
            .collect(Collectors.toCollection(ArrayList::new));
    List<ChangeStep.CacheId> redoHistory =
        redo.stream()
            .map(i -> new ChangeStep.CacheId(i))
            .collect(Collectors.toCollection(ArrayList::new));
    return new History(context, undoHistory, redoHistory, changeStep, maxUndo);
  }

  public History(
      ModelBase context,
      List<ChangeStep.CacheId> undoHistory,
      List<ChangeStep.CacheId> redoHistory,
      ChangeStep changeStep,
      int maxUndo) {
    this.context = context;
    this.maxUndo = maxUndo;
    this.changeStep = changeStep;
    this.undoHistory = undoHistory;
    this.redoHistory = redoHistory;
    context.committer.setDirty(this.changeStep);
  }

  public void addUndoSizeListener(Consumer<Integer> listener) {
    undoSizeListeners.add(listener);
    listener.accept(undoHistory.size());
  }

  public void removeUndoSizeListener(Consumer<Integer> listener) {
    undoSizeListeners.remove(listener);
  }

  public void addRedoSizeListener(Consumer<Integer> listener) {
    redoSizeListeners.add(listener);
    listener.accept(redoHistory.size());
  }

  public void removeRedoSizeListener(Consumer<Integer> listener) {
    redoSizeListeners.remove(listener);
  }

  private void notifyUndo() {
    new ArrayList<>(undoSizeListeners).forEach(c -> c.accept(undoHistory.size()));
    new ArrayList<>(redoSizeListeners).forEach(c -> c.accept(redoHistory.size()));
  }

  private Instant lastChangeTime = Instant.EPOCH;
  private History.Tuple lastChangeUnique = null;

  public void change(Tuple unique, List<Runnable> postChange, Consumer<ChangeStep> consumer) {
    wrapHistory(
        () -> {
          Instant now = Instant.now();
          if (unique == null
              || lastChangeUnique == null
              || !unique.equals(lastChangeUnique)
              || now.isAfter(lastChangeTime.plusSeconds(1))) {
            lastChangeUnique = null;
            innerFinishChange(postChange);
          }
          lastChangeTime = now;
          lastChangeUnique = unique;
          innerChange(consumer, postChange);
        });
    notifyUndo();
  }

  private void wrapHistory(Common.UncheckedRunnable inner) {
    History.Tuple historicUnique = lastChangeUnique;
    try {
      inner.run();
    } catch (History.InChangeError e) {
      throw new Assertion(
          String.format(
              "Attempting concurrent changes! In %s, new change is %s.\n",
              historicUnique, lastChangeUnique));
    } catch (Exception e) {
      throw uncheck(e);
    }
  }

  public static class InChangeError extends Exception {}

  private void innerChange(Consumer<ChangeStep> cb, List<Runnable> postChange)
      throws InChangeError {
    if (inChange) throw new InChangeError();
    clearRedo();
    inChange = true;
    context.lock.writeLock().lock();
    ChangeStep partial = new ChangeStep(null);
    try {
      cb.accept(partial);
    } catch (RuntimeException e) {
      logger.writeException(e, "Change failed, rewinding");
      List<Runnable> postChange1 = new ArrayList<>(); // Discarded on reverse
      partial.apply(context, postChange1).remove(context, postChange1); // Undo partial change
      context.debugCheckRefs();
      throw new RuntimeException("Change failed");
    } finally {
      context.lock.writeLock().unlock();
      inChange = false;
    }
    for (Change change1 : partial.changes) this.changeStep.add(context, change1, postChange);
    context.debugCheckRefs();
    context.committer.setDirty(this.changeStep);
    context.committer.setDirty(context);
  }

  private void innerFinishChange(List<Runnable> postChange) throws InChangeError {
    if (inChange) throw new InChangeError();
    if (changeStep.changes.isEmpty()) return;
    undoHistory.add(changeStep.cacheId);
    clearRedo();
    List<ChangeStep.CacheId> excessUndo =
        undoHistory.subList(0, Math.max(0, undoHistory.size() - maxUndo));
    excessUndo.forEach(c -> get(c).remove(context, postChange));
    excessUndo.clear();
    changeStep = new ChangeStep(new ChangeStep.CacheId(context.nextId++));
    context.committer.setDirty(changeStep);
  }

  public ChangeStep getNullable(ChangeStep.CacheId id) {
    return getNullable(context, id);
  }

  public static ChangeStep getNullable(ModelBase context, ChangeStep.CacheId id) {
    return get(context, id, true);
  }

  public ChangeStep get(ChangeStep.CacheId id) {
    return get(context, id);
  }

  public static ChangeStep get(ModelBase context, ChangeStep.CacheId id) {
    return get(context, id, false);
  }

  public ChangeStep get(ChangeStep.CacheId id, boolean nullable) {
    return get(context, id, nullable);
  }

  public static ChangeStep get(ModelBase context, ChangeStep.CacheId id, boolean nullable) {
    Path path = ChangeStep.path(context, id.id);
    try {
      {
        ChangeStep out = stepLookup.get(id);
        if (out != null) return out;
      }
      return uncheck(
          () -> {
            try (InputStream source = Files.newInputStream(path)) {
              ChangeStep out = new ChangeStep(id);
              out.changes =
                  new StackReader()
                      .read(
                          source,
                          new StackReader.ArrayState() {
                            @Override
                            public void value(Object value) {
                              if (!(value instanceof Change)) throw new Assertion();
                              data.add(value);
                            }

                            @Override
                            public StackReader.State record() {
                              if (type == null)
                                throw new IllegalStateException("Change has no type!");
                              return context.deserializeChange(type);
                            }
                          });
              return out;
            }
          });
    } catch (Exception e) {
      if (nullable) {
        logger.writeException(e, "Failed to deserialize change %s", path);
        return null;
      }
      throw e;
    }
  }

  private void clearRedo() {
    List<Runnable> postChange = new ArrayList<>();
    for (ChangeStep.CacheId c : redoHistory) {
      get(c).remove(context, postChange);
    }
    redoHistory.clear();
    for (Runnable runnable : postChange) {
      runnable.run();
    }
  }

  private void innerClearHistory() throws InChangeError {
    List<Runnable> postChange = new ArrayList<>();
    innerFinishChange(postChange);
    for (ChangeStep.CacheId c : undoHistory) get(c).remove(context, postChange);
    undoHistory.clear();
    clearRedo();
    for (Runnable runnable : postChange) {
      runnable.run();
    }
  }

  private void innerUndo() throws InChangeError {
    List<Runnable> postChange = new ArrayList<>();
    innerFinishChange(postChange);
    if (undoHistory.isEmpty()) return;
    ChangeStep.CacheId selectedId = undoHistory.remove(undoHistory.size() - 1);
    ChangeStep selected = get(selectedId);
    ChangeStep redo = selected.apply(context, postChange);
    redoHistory.add(redo.cacheId);
    context.committer.setDirty(redo);
    context.committer.setDirty(context);
    context.debugCheckRefs();
    for (Runnable runnable : postChange) {
      runnable.run();
    }
  }

  private void innerRedo() throws InChangeError {
    List<Runnable> postChange = new ArrayList<>();
    innerFinishChange(postChange);
    if (redoHistory.isEmpty()) return;
    ChangeStep.CacheId selectedId = redoHistory.remove(redoHistory.size() - 1);
    ChangeStep selected = get(selectedId);
    ChangeStep undo = selected.apply(context, postChange);
    context.committer.setDirty(undo);
    context.committer.setDirty(context);
    undoHistory.add(undo.cacheId);
    context.debugCheckRefs();
    for (Runnable runnable : postChange) {
      runnable.run();
    }
  }

  public void undo() {
    wrapHistory(
        () -> {
          lastChangeUnique = null;
          innerUndo();
        });
    notifyUndo();
  }

  public void redo() {
    wrapHistory(
        () -> {
          lastChangeUnique = null;
          innerRedo();
        });
    notifyUndo();
  }

  public void finishChange() {
    List<Runnable> postChange = new ArrayList<>();
    wrapHistory(
        () -> {
          lastChangeUnique = null;
          innerFinishChange(postChange);
        });
    notifyUndo();
    for (Runnable runnable : postChange) {
      runnable.run();
    }
  }

  public void clearHistory() {
    wrapHistory(
        () -> {
          lastChangeUnique = null;
          innerClearHistory();
        });
    notifyUndo();
  }

  public void serialize(RawWriter writer) {
    writer.key("undo").arrayBegin();
    undoHistory.forEach(c -> writer.primitive(Objects.toString(c.id)));
    writer.arrayEnd();
    writer.key("redo").arrayBegin();
    redoHistory.forEach(c -> writer.primitive(Objects.toString(c.id)));
    writer.arrayEnd();
  }

  public static class Tuple {
    final List data;

    public Tuple(Object... data) {
      this.data = Arrays.asList(data);
    }

    @Override
    public boolean equals(Object obj) {
      Tuple other = (Tuple) obj;
      if (data.size() != other.data.size()) return false;
      for (int i = 0; i < data.size(); ++i)
        if (!Objects.equals(data.get(i), other.data.get(i))) return false;
      return true;
    }
  }
}
